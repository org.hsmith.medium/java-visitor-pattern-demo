# Demonstration of Visitor Pattern

Demo project to show how the Visitor Pattern can be used in Java.

In this example the `Car` interface can be visited by implementations of `CarVisitor`.
An `Inspector` object is a kind of `CarVisitor` that is responsible for building the correct mechanic for the provided car.
The `Garage` object orchestrates the whole thing and stores the inspector used to build mechanics.

The `UnderwriterImpl` class was added to show how more types of `CarVisitor` can be implemented to provide completely different functionality without making changes to the `Car` interface.