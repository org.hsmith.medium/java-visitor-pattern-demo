package org.hsmith.medium;

interface CarVisitor {
    void visit(Car objectToVisit);
    void visit(PetrolCar objectToVisit);
    void visit(ElectricCar objectToVisit);
}

interface CarInspector extends CarVisitor {
    Mechanic findMechanicForCar(Car car);
}

class CarInspectorImpl implements CarInspector {
    private Mechanic mechanic;

    public Mechanic findMechanicForCar(Car car) {
        car.accept(this);
        return mechanic;
    }

    public void visit(Car objectToVisit) {
        System.out.println("Inspector found GENERAL car");
        mechanic = new GeneralMechanicImpl();
    }

    public void visit(PetrolCar objectToVisit) {
        System.out.println("Inspector found PETROL car");
        mechanic = new PetrolCarMechanicImpl();
    }

    public void visit(ElectricCar objectToVisit) {
        System.out.println("Inspector found ELECTRIC car");
        mechanic = new ElectricCarMechanicImpl();
    }
}

class UnderwriterImpl implements CarVisitor {
    private double premium = 0.0;

    public double calculatePremiumForCar(Car car) {
        car.accept(this);
        return premium;
    }

    public void visit(Car objectToVisit) {
        System.out.println("Underwriter found GENERAL car");
        premium = 100.0;
    }

    public void visit(PetrolCar objectToVisit) {
        System.out.println("Underwriter found PETROL car");
        premium = 200.0;
    }

    public void visit(ElectricCar objectToVisit) {
        System.out.println("Underwriter found ELECTRIC car");
        premium = 50.0;
    }
}
