package org.hsmith.medium;

interface Car {
    void accept(CarVisitor visitor);
}

class GeneralCarImpl implements Car {
    public void accept(CarVisitor visitor) {
        visitor.visit(this);
    }
}

interface PetrolCar extends Car {
}

class PetrolCarImpl extends GeneralCarImpl implements PetrolCar {
    @Override
    public void accept(CarVisitor visitor) {
        visitor.visit(this);
    }
}

interface ElectricCar extends Car {
}

class ElectricCarImpl extends GeneralCarImpl implements ElectricCar {
    @Override
    public void accept(CarVisitor visitor) {
        visitor.visit(this);
    }
}