package org.hsmith.medium;

interface Garage {
    void fixCar(Car carToFix);
}

class GarageImpl implements Garage {
    private CarInspector inspector;

    GarageImpl() {
        inspector = new CarInspectorImpl();
    }

    public void fixCar(Car carToFix) {
        Mechanic mechanic = inspector.findMechanicForCar(carToFix);
        mechanic.fixCar(carToFix);
    }
}