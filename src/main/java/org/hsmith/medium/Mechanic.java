package org.hsmith.medium;

interface Mechanic<TCarType extends Car> {
    void fixCar(TCarType carToFix);
}

class GeneralMechanicImpl implements Mechanic<Car> {
    public void fixCar(Car carToFix) {
        System.out.println("General mechanic fixing car");
    }
}

interface PetrolCarMechanic extends Mechanic<PetrolCar> {
}

class PetrolCarMechanicImpl implements PetrolCarMechanic {
    public void fixCar(PetrolCar carToFix) {
        System.out.println("Petrol car mechanic fixing car");
    }
}

interface ElectricCarMechanic extends Mechanic<ElectricCar> {
}

class ElectricCarMechanicImpl implements ElectricCarMechanic {
    public void fixCar(ElectricCar carToFix) {
        System.out.println("Electric car mechanic fixing car");
    }
}