package org.hsmith.medium;

public class App {
    public static void main(String[] args) {
        Garage garage = new GarageImpl();

        Car petrolCar = new PetrolCarImpl();
        garage.fixCar(petrolCar);

        System.out.println("----");

        Car electricCar = new ElectricCarImpl();
        garage.fixCar(electricCar);

        System.out.println("----");

        Car generalCar = new GeneralCarImpl();
        garage.fixCar(generalCar);

        System.out.println("----");

        UnderwriterImpl underwriter = new UnderwriterImpl();
        System.out.println("Premium for electric car: " + underwriter.calculatePremiumForCar(electricCar));
    }
}
